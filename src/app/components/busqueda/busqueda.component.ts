import { ActivatedRoute } from '@angular/router';
import { HeroesService } from './../../service/heroes.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-busqueda',
  templateUrl: './busqueda.component.html',
  styleUrls: ['./busqueda.component.css']
})
export class BusquedaComponent implements OnInit {

  heroes: any[] = [];

  constructor(private heroeService: HeroesService, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe( params =>{
      this.heroes = this.heroeService.buscarHeroe( params['termino'] );
      console.log( this.heroes );
    });
  }
}
